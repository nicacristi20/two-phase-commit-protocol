import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class ParticipantClass {

    private DataOutputStream dos;
    private static DataInputStream dis;
    private Socket clientsoc;
    private static String clientname;

    private String state = "INIT";
    private String data = "";
    private String decision = "";

    private boolean connected = false;

    public String regex = "^[a-zA-Z0-9]+$";

    /*Static Http variables used for building http request headers*/
    private final static String host = "Host: localhost";
    private final static String userAgent = "User-Agent: MultiChat/2.0";
    private final static String contentType = "Content-Type: text/html";
    private final static String contentlength = "Content-Length: ";
    private final static String date = "Date: ";

    /*These 4 file reader/writer variables used for Server Logging*/
    private FileWriter fw;
    private BufferedWriter bw;
    private FileReader fr;
    private BufferedReader br;

    private Timer t;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ParticipantClass window = new ParticipantClass(args[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public ParticipantClass(String participantName) {

        initialize(participantName);
    }

    private void initialize(String participantName) {

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    String line = scanner.nextLine();
                    if (line.equals("COMMIT")) {
                        if ((state.equals("READY") || (data.trim().isEmpty() || data.equals("")))) {
                            JOptionPane.showMessageDialog(null, "Can't Vote Now !");
                        } else {

                            try {
                                StringBuilder sbconnreq = new StringBuilder();

							/*Building the Http Connection Request and passing Client name as body. Thus the Http Header
							are encoded around the client name data.*/
                                sbconnreq.append("POST /").append("COMMIT").append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                                        append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(clientname.length()).append("\r\n").
                                        append(date).append(new Date()).append("\r\n");

                                dos.writeUTF(sbconnreq.toString());
                                System.out.println(Constants.ANSI_GREEN + "Voted to: COMMIT" + Constants.ANSI_RESET);

                                /*state transistion to Ready state*/
                                state = "READY";
                                decision = "";

							/*Initiating a new timer to keep track of decision of coordinator and times out
							if it doesnt arrive in stipulated time*/
                                t = new Timer();

							/*initiating a timer task to be associated with timer which performs the actual check
							of whether the decision has arrived or not*/
                                TimerTask tt2 = new TimerTask() {

                                    @Override
                                    public void run() {
                                        /*check to see whether decision has arrived or not*/
                                        if (!(decision.equals("g_c") || decision.equals("g_a"))) {
                                            System.out.println(Constants.ANSI_RED + "Did not receive decision from coordinator.. SO LOCAL ABORT !" + Constants.ANSI_RESET);
                                            data = "";
                                            state = "ABORT";
                                        }
                                    }
                                };
                                /*scheduling this check task every one minute*/
                                t.schedule(tt2, 60000);

                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }


                        }
                    } else if (line.equals("ABORT")) {
                        if (!(data.equals("") || data.trim().isEmpty())) {

                            try {
                                /*String builder object to encode the message in Http Request format*/
                                StringBuilder sbconnreq = new StringBuilder();

						/*Building the Http Connection Request and passing Client name as body. Thus the Http Header
					are encoded around the client name data.*/
                                sbconnreq.append("POST /").append("ABORT").append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                                        append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(clientname.length()).append("\r\n").
                                        append(date).append(new Date()).append("\r\n");

                                dos.writeUTF(sbconnreq.toString());

                                System.out.println(Constants.ANSI_RED + "Voted to: ABORT" + Constants.ANSI_RESET);
                                state = "ABORT";
                                data = "";

                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else {
                            System.out.println(Constants.ANSI_RED + "Can't Vote Now !" + Constants.ANSI_RESET);
                        }
                    } else {
                        System.out.println("COMMAND NOT FOUND");
                    }
                }
            }
        };
        Thread mainThread = new Thread(r);
        mainThread.start();
        connectParticipant(participantName);
    }

    private void connectParticipant(String participantName) {
        if (connected == true) {
            System.out.println(Constants.ANSI_YELLOW + "You are already connected !" + Constants.ANSI_RESET);
        } else if (connected == false) {

            clientname = participantName;

            /*Checking for bad client usernames and accepting only alphanumeric names*/
            if (clientname.equals(null) || clientname.trim().isEmpty() || (!Pattern.matches(regex, clientname))) {
                System.out.println(Constants.ANSI_RED + "Please enter an alphanumeric username to connect to server! " + Constants.ANSI_RESET);
                System.exit(-1);
            } else {

                /*calling the method to start client connection.*/
                startClientConnection();

                /*Initiating a new timer to keep track of voting request of coordinator and times out
                if it doesn't arrive in stipulated time*/
                t = new Timer();

                /*initiating a timer task to be associated with timer which performs the actual check
                of whether the voting request has arrived or not*/
                TimerTask tt = new TimerTask() {

                    @Override
                    public void run() {
                        if (data.equals("") && state.equals("INIT")) {
                            System.out.println(Constants.ANSI_RED + "Did Not get Voting Request...Local Abort!" + Constants.ANSI_RESET);
                            state = "ABORT";
                        }
                    }
                };
                /*scheduling this check task in one minute(60000 miliseconds)*/
                t.schedule(tt, 35000);
            }
        }
    }

    /*Requests the server for Connection by creating as stream socket fotr the server port number- 9998*/
    private void startClientConnection() {

        try {

            /*making connection request*/
            clientsoc = new Socket(Constants.IP, Constants.SERVER_PORT);

            /*Input and output streams for data sending and receiving through client and server sockets.*/
            dis = new DataInputStream(clientsoc.getInputStream());
            dos = new DataOutputStream(clientsoc.getOutputStream());

            /*Creating a new file if it does not exist for each participant this acts as non volatile memory*/
            File f = new File(clientname);
            f.createNewFile();

            StringBuilder sbconnreq = new StringBuilder();

			/*Building the Http Connection Request and passing Client name as body. Thus the Http Header
			are encoded around the client name data.*/
            sbconnreq.append("GET /").append("{" + clientname + "}").append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                    append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(clientname.length()).append("\r\n").
                    append(date).append(new Date()).append("\r\n");

            dos.writeUTF(sbconnreq.toString());

            System.out.println(Constants.ANSI_GREEN + "You have logged in!" + Constants.ANSI_RESET);
            connected = true;

            fr = new FileReader(f);
            br = new BufferedReader(fr);

            String smh = "";
            while (!((smh = br.readLine()) == null)) {
                System.out.println(Constants.ANSI_YELLOW + "Previously Commited Data: \n" + smh + Constants.ANSI_RESET);
            }
            br.close();

            /*File writer to write into participant's file, the commited data*/
            fw = new FileWriter(f, true);
            bw = new BufferedWriter(fw);
        } catch (FileNotFoundException e) {
            System.out.println(Constants.ANSI_RED + "File not located..exception" + Constants.ANSI_RESET);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Server Down. Couldn't Connect");
        }

        new ParticipantThread().start();
    }

    /*Nested Class of ParticipantClass, this class extends Thread Class because this is used as a dedicated thread
    to handle incoming messages from coordinator and server such as Vote Request, Decision etc*/
    public class ParticipantThread extends Thread {

		/*overriding the thread's run method. this method runs as soon as we send command:
		thread.start();*/

        @Override
        public void run() {

            String readChat = "";
            String arr[], msgin;

            try {

                while (true) {

                    readChat = dis.readUTF();

					/*Deconstructing the HTTP message from server
					and decoding it from http format to read the contents*/
                    arr = readChat.split("\n");

                    msgin = arr[0].split("/")[1];

                    /*Decoding for POST type messages from Server*/
                    if (arr[0].contains("POST")) {

                        /*In case decision is global commit*/
                        if (msgin.equalsIgnoreCase("GLOBAL_COMMIT")) {
                            decision = "g_c";

                            /*State Transition from Ready to Commit after decision of coordinator*/
                            state = "COMMIT";
                            System.out.println(Constants.ANSI_GREEN + "Commiting due to Global Commit!" + Constants.ANSI_RESET);

							/*In case of Global Commit, We write this string to non volatile memory
							  i.e. File in this case*/
                            bw.write(data);
                            bw.newLine();
                            bw.flush();

                            /*Set the reveived data to empty as this has been commited now*/
                            data = "";
                        }
                        /*In case decision is global abort*/
                        else if (msgin.equalsIgnoreCase("GLOBAL_ABORT")) {

                            System.out.println(Constants.ANSI_RED + "Aborting due to Global Abort!" + Constants.ANSI_RESET);

                            /*State Transition from Ready to Abort after decision of coordinator*/
                            state = "ABORT";
                            decision = "g_a";

                            data = "";

                        } else {
                            /*This is the Vote request from coordinator so decoding it*/
                            data = msgin.split(":")[0];
                            System.out.println(Constants.ANSI_YELLOW + "VOTE to COMMIT OR ABORT the string: " + data + Constants.ANSI_RESET);

                            /*Stopping timer as participant has received the Vote Request within stipulated time*/
                            t.cancel();
                            t.purge();
                        }
                    }


                }
            }
            /*When connection with server has lost due to server crash*/ catch (Exception e) {
                System.out.println(Constants.ANSI_RED + "SERVER DOWN...." + Constants.ANSI_RESET);
            }

        }
    }
}
