import javax.swing.*;
import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Timer;
import java.util.*;

public class CoordinatorClass {
    /*Ip address of server*/
    private static Socket coordinatorSoc;
    private static DataInputStream dis;
    private static DataOutputStream dos;

    /*flag to maintain state of coordinator*/
    private String state;

    //Unique Id of coordinator
    private static final String COORDINATOR_NAME = "COORDINATOR_DA";

    /*Various flags to represent diffferent states of Coordinator*/
    private static final String init = "INIT",
            waiting = "WAITING",
            commit = "COMMIT",
            abort = "ABORT";

    /*Array-list to store the usernames of clients online*/
    private ArrayList<String> userNames;

    /*Array List to store the names of participants to remove(if they get disconnected)
    after each transaction*/
    private ArrayList<String> toRemove;

    /*Hashmap to store the votes of all participants*/
    private HashMap<String, String> votes;

    private boolean allVoted = false;

    /*Timer to calculate the 2PC timeout scenarios of coordinator*/
    private Timer t;

    /*Static Http variables used for building http request headers*/
    private final static String host = "Host: localhost";
    private final static String userAgent = "User-Agent: MultiChat/2.0";
    private final static String contentType = "Content-Type: text/html";
    private final static String contentlength = "Content-Length: ";
    private final static String date = "Date: ";

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CoordinatorClass window = new CoordinatorClass();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public CoordinatorClass() {
        initialize();
    }

    private void initialize() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Scanner scanner = new Scanner(System.in);
                while (true) {
                    String line = scanner.nextLine();
                    if (line.startsWith("SEND:")) {
                        String msg = line.split(":")[1];
                        sendMsg(msg);
                    } else if (line.equals("USERS")) {
                        if (userNames.isEmpty()) {
                            System.out.println("No User is online.");
                        } else {
                            System.out.println("Online users:");
                            for (String user : userNames) {
                                System.out.println(user);
                            }
                        }
                    } else if (line.equals("STATE")) {
                        System.out.println("STATE: " + state);
                    } else {
                        System.out.println("COMMAND NOT FOUND");
                    }
                }
            }
        };
        Thread t = new Thread(r);
        t.start();

        startClientConnection();
    }

    private void sendMsg(String msg) {
        try {
            StringBuilder sbvotereq = new StringBuilder();

            /*Encoding the vote request into HTTP format to be sent across the network*/
            sbvotereq.append("POST /").append(msg + ":VOTE_REQUEST").append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                    append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(COORDINATOR_NAME.length()).append("\r\n").
                    append(date).append(new Date()).append("\r\n");

            /*Sending the vote request across data output stream of coordinator socket*/
            dos.writeUTF(sbvotereq.toString());

            /*COORDINATOR STATE TRANSITION FROM INIT TO WAITING*/
            state = waiting;

            allVoted = false;

            /*Initializing the timer to calculate timeout in case all votes do not come*/
            t = new Timer();

            /*Making a timer task to perform the function of checking whether all votes have arrived after a particular time period*/
            TimerTask tt1 = new TimerTask() {

                @Override
                public void run() {

                    int votecount = 0;
                    for (String participant : votes.keySet()) {
                        if (votes.get(participant).equals("COMMIT")) {
                            votecount++;
                        } else {
                            break;
                        }
                    }
                    /*check to see if all participants have voted or not*/
                    if (votecount != votes.size()) {
                        System.out.println(Constants.ANSI_RED + "Time Out. Not all participants voted." + Constants.ANSI_RESET);
                        System.out.println(Constants.ANSI_RED + "Initiating Global Abort." + Constants.ANSI_RESET);

                        StringBuilder sbglobalabort = new StringBuilder();

                        sbglobalabort.append("POST /").append("GLOBAL_ABORT:" + COORDINATOR_NAME).append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                                append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(COORDINATOR_NAME.length()).append("\r\n").
                                append(date).append(new Date()).append("\r\n");

                        try {
                            dos.writeUTF(sbglobalabort.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        while (!toRemove.isEmpty()) {
                            System.out.println("removed these participant now: " + toRemove.get(0) + "\n");
                            userNames.remove(toRemove.get(0));
                            votes.remove(toRemove.remove(0));
                        }
                        toRemove.clear();

                        for (String client : votes.keySet()) {
                            votes.put(client, "");
                        }
                        state = abort;
                    }


                }
            };
            /*scheduling this check task 45 seconds*/
            t.schedule(tt1, 45000);

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /*Requests the server for Connection by creating as stream socket for the server port number*/
    private void startClientConnection() {

        try {

            /*making connection request*/
            coordinatorSoc = new Socket(Constants.IP, Constants.SERVER_PORT);

            userNames = new ArrayList<>();
            votes = new HashMap<>();
            toRemove = new ArrayList<>();

            state = init;

            /*Input and output streams for data sending and receiving through client and server sockets.*/
            dis = new DataInputStream(coordinatorSoc.getInputStream());
            dos = new DataOutputStream(coordinatorSoc.getOutputStream());

            /*String builder object to encode the message in Http Request format*/
            StringBuilder sbconnreq = new StringBuilder();

			/*Building the Http Connection Request and passing Client name as body. Thus the Http Header
			are encoded around the client name data.*/
            sbconnreq.append("GET /").append("{" + COORDINATOR_NAME + "}").append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                    append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(COORDINATOR_NAME.length()).append("\r\n").
                    append(date).append(new Date()).append("\r\n");

            dos.writeUTF(sbconnreq.toString());

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Server Down. Couldn't Connect");
        }

        new CoordinatorThread().start();
    }


    /*Nested Class of CoordinatorClass, this class extends Thread Class because this is used as a dedicated thread
    to handle incoming messages from server and clients such as Votes, events such as participant login etc*/
    public class CoordinatorThread extends Thread {
        @Override
        public void run() {

            String line = "", msgin, data;
            String arr[];

            try {

                while ((line = dis.readUTF()) != null) {

                    arr = line.split("\n");

                    msgin = arr[0].split("/")[1];

					/*Reconstructing the message body from the Http Header.
					  This code decodes the Http message body.*/
                    if (arr[0].contains("POST")) {

                        /*new participant connected*/
                        if (msgin.contains("CONNECTED")) {

                            data = msgin.split(":")[1];

                            if (!(data.equalsIgnoreCase(COORDINATOR_NAME))) {

                                /*keeping participants in list to keep track*/
                                userNames.add(data);

                                /*initially keeping all votes as empty as transaction hasn't begun yet*/
                                votes.put(data, "");

                                System.out.println(Constants.ANSI_GREEN + "new participant : " + data + Constants.ANSI_RESET);
                            }

                        }
                        /*participant has voted to abort*/
                        else if (msgin.contains("ABORT")) {

                            data = msgin.split(":")[1];
                            votes.put(data, "ABORT");
                            state = abort;
                            System.out.println(Constants.ANSI_RED +
                                    "Abort Vote by client: " + data + " " + "One client voted to Abort so initiating GLOBAL ABORT !" +
                                    Constants.ANSI_RESET);
                            //		Thread.sleep(2000);

                            t.cancel();
                            t.purge();

                            /*String builder object to encode the message in Http Request format*/
                            StringBuilder sbglobalabort = new StringBuilder();

                            sbglobalabort.append("POST /").append("GLOBAL_ABORT:" + COORDINATOR_NAME).append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                                    append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(COORDINATOR_NAME.length()).append("\r\n").
                                    append(date).append(new Date()).append("\r\n");

                            dos.writeUTF(sbglobalabort.toString());

                            while (!toRemove.isEmpty()) {
                                //textArea.append("removed these participant now: " + toRemove.get(0));
                                System.out.println(Constants.ANSI_RED + "removed these participant now: " + toRemove.get(0) + Constants.ANSI_RESET);
                                userNames.remove(toRemove.get(0));
                                votes.remove(toRemove.remove(0));
                            }
                            toRemove.clear();

                            for (String client : votes.keySet()) {
                                votes.put(client, "");
                            }
                        }
                        /*Commit message by participant*/

                        else if (msgin.contains("COMMIT")) {

                            data = msgin.split(":")[1];
                            votes.put(data, "COMMIT");
                            System.out.println(Constants.ANSI_GREEN + "Commit Vote by client: " + data + Constants.ANSI_RESET);

                            int counter = 0;
                            for (String key : votes.keySet()) {
                                if (votes.get(key).equals("COMMIT")) {
                                    counter++;
                                } else {
                                    break;
                                }
                            }
                            /*check to see if all participants have voted*/
                            if (counter == (votes.size())) {

                                t.cancel();
                                t.purge();
                                System.out.println(Constants.ANSI_GREEN +
                                        "All participants voted to commit..so initiating global commit." + Constants.ANSI_RESET);
                                state = commit;

                                /*String builder object to encode the message in Http Request format*/
                                StringBuilder sbglobalcom = new StringBuilder();

                                sbglobalcom.append("POST /").append("GLOBAL_COMMIT:" + COORDINATOR_NAME).append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                                        append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(COORDINATOR_NAME.length()).append("\r\n").
                                        append(date).append(new Date()).append("\r\n");

                                /*Pausing this thread to function to test coordinator crash scenario.*/
                                Thread.sleep(2000);

                                dos.writeUTF(sbglobalcom.toString());

                                while (!toRemove.isEmpty()) {
                                    System.out.println(Constants.ANSI_YELLOW + "removed these participant now: " + toRemove.get(0) + Constants.ANSI_RESET);
                                    userNames.remove(toRemove.get(0));
                                    votes.remove(toRemove.remove(0));
                                }
                                toRemove.clear();

                                for (String client : votes.keySet()) {
                                    votes.put(client, "");
                                }

                            }
                        }

                        /*Logic for coordinator to know the current participants list in case it resumes after crashing*/
                        else if (msgin.contains("USER_LIST")) {

                            data = msgin.split(":")[1];
                            String[] userss = data.split(",");
                            String cname = "";

                            for (int i = 0; i < userss.length; i++) {
                                cname = userss[i];

                                if (cname.contains("[") || cname.contains(" ")) {
                                    cname = cname.replace(cname.substring(0, 1), "");
                                }
                                if (cname.contains("]")) {
                                    cname = cname.replace(cname.substring(cname.length() - 1), "");
                                }

                                /*adding participant's to array list to keep track
                                 */
                                userNames.add(cname);
                                /*initially keeping all votes as empty as transaction hasn't begun yet
                                 */
                                votes.put(cname, "");
                            }
                        } else {
							/*this participant has been disconnected so it will be removed from coordinator's list
							after the current transaction*/
                            data = msgin.split(":")[1];

                            if (!(state == waiting)) {
                                userNames.remove(data);
                                votes.remove(data);
                            } else {
                                toRemove.add(data);
                            }
                        }
                    }
                }

            } catch (InterruptedException ie) {
            } catch (IOException e) {
            }
        }
    }
}
