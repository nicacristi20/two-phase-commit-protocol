import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

public class ServerClass {
    private static ServerSocket ss;
    private static Socket client;
    private static DataOutputStream dos;
    //Unique Id of coordinator
    private static final String COORDINATOR_NAME = "COORDINATOR_DA";

    /*Array-list to store the usernames of clients online*/
    private ArrayList<String> userNames;

    /*Array-list to store the data-output streams of online clients
     in order for server to broadcast messages*/
    private ArrayList<DataOutputStream> streams;
    private DataOutputStream coordinatorStream;

    /*Static Http variables used for building http request headers*/
    private final static String host = "Host: localhost";
    private final static String userAgent = "User-Agent: MultiChat/2.0";
    private final static String contentType = "Content-Type: text/html";
    private final static String contentlength = "Content-Length: ";
    private final static String date = "Date: ";

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ServerClass window = new ServerClass();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /*Constructor of Server Class which initializes the GUI Frame*/
    public ServerClass() {

        /*calls the method to initialize the contents of GUI Frame*/
        initialize();
    }

    /*Method to Initialize the contents of the Swing GUI frame(Server).*/
    private void initialize() {

		/*This sole thread is dedicated for starting and maintaining server connection.
		  It calls the startsServerConnection method that actually initializes the server.*/
        Thread t = new Thread() {

            @Override
            public void run() {
                startServerConnection();
            }

            ;
        };
        t.start();
    }

    /*Method to initialize the server connection.*/
        protected void startServerConnection() {

        try {
            ss = new ServerSocket(Constants.SERVER_PORT);
            userNames = new ArrayList<>();
            streams = new ArrayList<>();
            System.out.println(Constants.ANSI_GREEN + "-------SERVER STARTED------" + Constants.ANSI_RESET);


            /*This loop is used for listening client connections on server port*/
            while (true) {

                /*Client has connected to server socket*/
                client = ss.accept();

                dos = new DataOutputStream(client.getOutputStream());
                streams.add(dos);

                ServerClientHandler sch = new ServerClientHandler(client, dos);

                /*Initiate the thread for handling this sole client session*/
                sch.start();
            }
        } catch (IOException e) {
            e.getMessage();
        }
    }

    /*Method to send the messages/decisions/votes to all participants*/
    private void sendDataAllParticipants(String msg) {
        StringBuilder sbr = new StringBuilder();

        for (DataOutputStream dataOutputStream : streams) {
            try {

                sbr.append("POST /").append(msg).append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                        append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(msg.length()).append("\r\n").
                        append(date).append(new Date()).append("\r\n");

                dataOutputStream.writeUTF(sbr.toString());

            } catch (Exception e) {
            }
        }

    }

    /*Method to send the messages/votes/etc data to coordinator*/
    private void sendCoordinator(String data) {
        StringBuilder sbr = new StringBuilder();

        try {
            sbr.append("POST /").append(data).append("/ HTTP/1.1\r\n").append(host).append("\r\n").
                    append(userAgent).append("\r\n").append(contentType).append("\r\n").append(contentlength).append(data.length()).append("\r\n").
                    append(date).append(new Date()).append("\r\n");

            coordinatorStream.writeUTF(sbr.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /*Nested Class used for MultiThreading and Handling multiple clients at same time.*/
    public class ServerClientHandler extends Thread {
        private Socket csoc;
        private String cname;
        private DataInputStream diss;

        /*Constructor of this Participant Handler Class, its parameters are client object
        and client socket's data outputstream*/
        public ServerClientHandler(Socket client, DataOutputStream dosss) {
            this.csoc = client;
            try {
                diss = new DataInputStream(csoc.getInputStream());
            } catch (Exception e) {
                e.getMessage();
            }
        }

        @Override
        public void run() {

            String line = "", msgin;
            String arr[];

            try {

                while ((line = diss.readUTF()) != null) {

                    System.out.println(Constants.ANSI_YELLOW + line + Constants.ANSI_RESET);

                    arr = line.split("\n");

                    msgin = arr[0].split("/")[1];

					/*Reconstructing the message body from the Http Header.
					  This code decodes the Http message body.*/
                    if (arr[0].contains("GET")) {

                        if (msgin.contains("{")) {
                            cname = msgin.split("\\{")[1];
                            cname = cname.replace(cname.substring(cname.length() - 1), "");

                            /*check to know this client is not coordinator*/
                            if (!cname.equalsIgnoreCase(COORDINATOR_NAME)) {

                                userNames.add(cname);
                                sendCoordinator("CONNECTED:" + cname + ":");

                            } else {

                                /*This is the coordinator*/
                                if (streams.size() == 1) {
                                    coordinatorStream = new DataOutputStream(streams.remove(0));
                                } else {
                                    coordinatorStream = new DataOutputStream(streams.remove(streams.size() - 1));
                                    sendCoordinator("USER_LIST:" + userNames.toString());
                                }
                            }

                        }
                    } else if (arr[0].contains("POST")) {

                        if (msgin.contains("VOTE_REQUEST")) {

                            sendDataAllParticipants(msgin);
                            System.out.println(Constants.ANSI_YELLOW + "Sent the vote request to participants." + Constants.ANSI_RESET);

                        } else if (msgin.equalsIgnoreCase("ABORT")) {
                            System.out.println(Constants.ANSI_RED + "Abort Vote by client: " + cname + Constants.ANSI_RESET);
                            sendCoordinator("ABORT" + ":" + cname + ":");

                        } else if (msgin.equals("COMMIT")) {
                            System.out.println(Constants.ANSI_GREEN + "Commit Vote by client: " + cname + Constants.ANSI_RESET);
                            sendCoordinator("COMMIT" + ":" + cname + ":");

                        } else if (msgin.contains("GLOBAL_ABORT")) {

                            String glab = msgin.split(":")[0];
                            sendDataAllParticipants(glab);

                        } else if (msgin.contains("GLOBAL_COMMIT")) {

                            String glcm = msgin.split(":")[0];
                            sendDataAllParticipants(glcm);
                        }

                    }
                }

            }
            /*In case client connection is disconnected*/
            catch (IOException e) {
                System.out.println(Constants.ANSI_RED + cname + " has Disconnected" + Constants.ANSI_RESET);
                if (!cname.equals(COORDINATOR_NAME)) {
                    sendCoordinator("LOGGEDOUT:" + cname + ":");
                    //	Removing the client's username from it's memory or arraylist
                    userNames.remove(cname);
                }

            }
        }
    }
}
