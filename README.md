# Two Phase Commit Protocol - DA Course

### Steps for running the project:

* clone the repository
* from the root directory of the project type:
	`javac src/*.java`
* go into the src folder
	* for running the server:
	`java ServerClass`
	* for running the coordinator
	`java CoordinatorClass`
	* for running one or more participants
	`java ParticipantClass PARTICIPANT_NAME`
* commands:
	* for Coordinator:
		* SEND:MESSAGE_HERE - send message to participants
		* STATUS - show status for coordinator
		* USERS - show users / participants
	* for Participant(s)
		* COMMIT
		* ABORT